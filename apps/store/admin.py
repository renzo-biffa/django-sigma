from django.contrib import admin
# main models
from store.models import Store


admin.site.register(Store)